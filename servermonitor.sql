/*
 Navicat Premium Data Transfer

 Source Server         : mariadb.home.mx
 Source Server Type    : MariaDB
 Source Server Version : 100238
 Source Host           : mariadb.home.mx:3306
 Source Schema         : servermonitor

 Target Server Type    : MariaDB
 Target Server Version : 100238
 File Encoding         : 65001

 Date: 17/05/2022 16:10:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for psm_config
-- ----------------------------
DROP TABLE IF EXISTS `psm_config`;
CREATE TABLE `psm_config`  (
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`key`) USING BTREE
) ENGINE = MyISAM AVG_ROW_LENGTH = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of psm_config
-- ----------------------------
INSERT INTO `psm_config` VALUES ('language', 'es_ES');
INSERT INTO `psm_config` VALUES ('proxy', '0');
INSERT INTO `psm_config` VALUES ('proxy_url', '');
INSERT INTO `psm_config` VALUES ('proxy_user', '');
INSERT INTO `psm_config` VALUES ('proxy_password', '');
INSERT INTO `psm_config` VALUES ('email_status', '0');
INSERT INTO `psm_config` VALUES ('email_from_email', '');
INSERT INTO `psm_config` VALUES ('email_from_name', '');
INSERT INTO `psm_config` VALUES ('email_smtp', '0');
INSERT INTO `psm_config` VALUES ('email_smtp_host', '');
INSERT INTO `psm_config` VALUES ('email_smtp_port', '');
INSERT INTO `psm_config` VALUES ('email_smtp_security', '');
INSERT INTO `psm_config` VALUES ('email_smtp_username', '');
INSERT INTO `psm_config` VALUES ('email_smtp_password', '');
INSERT INTO `psm_config` VALUES ('sms_status', '0');
INSERT INTO `psm_config` VALUES ('sms_gateway', 'mollie');
INSERT INTO `psm_config` VALUES ('sms_gateway_username', '');
INSERT INTO `psm_config` VALUES ('sms_gateway_password', '');
INSERT INTO `psm_config` VALUES ('sms_from', '');
INSERT INTO `psm_config` VALUES ('pushover_status', '0');
INSERT INTO `psm_config` VALUES ('pushover_api_token', '1046807636:AAGB6y3TiVzXVs7feZY1r83TGrKhowt-gEI');
INSERT INTO `psm_config` VALUES ('password_encrypt_key', 'aa002e15a1680afc93577f72a283fbc07ea37733');
INSERT INTO `psm_config` VALUES ('alert_type', 'status');
INSERT INTO `psm_config` VALUES ('log_status', '1');
INSERT INTO `psm_config` VALUES ('log_email', '0');
INSERT INTO `psm_config` VALUES ('log_sms', '0');
INSERT INTO `psm_config` VALUES ('log_pushover', '0');
INSERT INTO `psm_config` VALUES ('log_retention_period', '365');
INSERT INTO `psm_config` VALUES ('version', '3.2.0');
INSERT INTO `psm_config` VALUES ('version_update_check', '3.2.0');
INSERT INTO `psm_config` VALUES ('auto_refresh_servers', '20');
INSERT INTO `psm_config` VALUES ('show_update', '0');
INSERT INTO `psm_config` VALUES ('last_update_check', '1579661862');
INSERT INTO `psm_config` VALUES ('cron_running', '1');
INSERT INTO `psm_config` VALUES ('cron_running_time', '1630963022');

-- ----------------------------
-- Table structure for psm_log
-- ----------------------------
DROP TABLE IF EXISTS `psm_log`;
CREATE TABLE `psm_log`  (
  `log_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `server_id` int(11) UNSIGNED NOT NULL,
  `type` enum('status','email','sms','pushover') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `datetime` timestamp(0) NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 AVG_ROW_LENGTH = 195 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of psm_log
-- ----------------------------

-- ----------------------------
-- Table structure for psm_log_users
-- ----------------------------
DROP TABLE IF EXISTS `psm_log_users`;
CREATE TABLE `psm_log_users`  (
  `log_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`log_id`, `user_id`) USING BTREE
) ENGINE = MyISAM AVG_ROW_LENGTH = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of psm_log_users
-- ----------------------------

-- ----------------------------
-- Table structure for psm_servers
-- ----------------------------
DROP TABLE IF EXISTS `psm_servers`;
CREATE TABLE `psm_servers`  (
  `server_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `port` int(5) UNSIGNED NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` enum('ping','service','website') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'service',
  `pattern` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` enum('on','off') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'on',
  `error` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rtime` float(9, 7) NULL DEFAULT NULL,
  `last_online` datetime(0) NULL DEFAULT NULL,
  `last_check` datetime(0) NULL DEFAULT NULL,
  `active` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'yes',
  `email` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'yes',
  `sms` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'no',
  `pushover` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'yes',
  `warning_threshold` mediumint(1) UNSIGNED NOT NULL DEFAULT 1,
  `warning_threshold_counter` mediumint(1) UNSIGNED NOT NULL DEFAULT 0,
  `timeout` smallint(1) UNSIGNED NULL DEFAULT NULL,
  `website_username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `website_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`server_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 AVG_ROW_LENGTH = 111 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of psm_servers
-- ----------------------------

-- ----------------------------
-- Table structure for psm_servers_history
-- ----------------------------
DROP TABLE IF EXISTS `psm_servers_history`;
CREATE TABLE `psm_servers_history`  (
  `servers_history_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `server_id` int(11) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `latency_min` float(9, 7) NOT NULL,
  `latency_avg` float(9, 7) NOT NULL,
  `latency_max` float(9, 7) NOT NULL,
  `checks_total` int(11) UNSIGNED NOT NULL,
  `checks_failed` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`servers_history_id`) USING BTREE,
  UNIQUE INDEX `server_id_date`(`server_id`, `date`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 AVG_ROW_LENGTH = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of psm_servers_history
-- ----------------------------

-- ----------------------------
-- Table structure for psm_servers_uptime
-- ----------------------------
DROP TABLE IF EXISTS `psm_servers_uptime`;
CREATE TABLE `psm_servers_uptime`  (
  `servers_uptime_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `server_id` int(11) UNSIGNED NOT NULL,
  `date` datetime(0) NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL,
  `latency` float(9, 7) NULL DEFAULT NULL,
  PRIMARY KEY (`servers_uptime_id`) USING BTREE,
  INDEX `server_id`(`server_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 AVG_ROW_LENGTH = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of psm_servers_uptime
-- ----------------------------

-- ----------------------------
-- Table structure for psm_users
-- ----------------------------
DROP TABLE IF EXISTS `psm_users`;
CREATE TABLE `psm_users`  (
  `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'user\'s name, unique',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'user\'s password in salted and hashed format',
  `password_reset_hash` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'user\'s password reset code',
  `password_reset_timestamp` bigint(20) NULL DEFAULT NULL COMMENT 'timestamp of the password reset request',
  `rememberme_token` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'user\'s remember-me cookie token',
  `level` tinyint(2) UNSIGNED NOT NULL DEFAULT 20,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mobile` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pushover_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pushover_device` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `unique_username`(`user_name`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 15 AVG_ROW_LENGTH = 154 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of psm_users
-- ----------------------------
INSERT INTO `psm_users` VALUES (1, 'hcastillos', '$2y$10$NWLzE31wtVr.2vPjtGSjG.ut5HaQe3iW1zu/FH.bajgCc/abq4vxe', NULL, NULL, NULL, 10, 'Heber Castillo Santiago', '', '', '', 'hcastillos@sre.gob.mx');
INSERT INTO `psm_users` VALUES (10, 'magomeze', '$2y$10$DWhtR8HYuXjOmg0LzYdTAeexoyelG6Ullj65xK6DNj8kYlU91xV.a', NULL, NULL, NULL, 10, 'Marco Antonio Gomez Espinoza', '', '', '', 'magomeze@sre.gob.mx');

-- ----------------------------
-- Table structure for psm_users_preferences
-- ----------------------------
DROP TABLE IF EXISTS `psm_users_preferences`;
CREATE TABLE `psm_users_preferences`  (
  `user_id` int(11) UNSIGNED NOT NULL,
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`user_id`, `key`) USING BTREE
) ENGINE = MyISAM AVG_ROW_LENGTH = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of psm_users_preferences
-- ----------------------------
INSERT INTO `psm_users_preferences` VALUES (3, 'status_layout', '0');
INSERT INTO `psm_users_preferences` VALUES (2, 'status_layout', '0');
INSERT INTO `psm_users_preferences` VALUES (6, 'status_layout', '1');
INSERT INTO `psm_users_preferences` VALUES (1, 'status_layout', '0');
INSERT INTO `psm_users_preferences` VALUES (10, 'status_layout', '1');
INSERT INTO `psm_users_preferences` VALUES (9, 'status_layout', '1');
INSERT INTO `psm_users_preferences` VALUES (12, 'status_layout', '1');

-- ----------------------------
-- Table structure for psm_users_servers
-- ----------------------------
DROP TABLE IF EXISTS `psm_users_servers`;
CREATE TABLE `psm_users_servers`  (
  `user_id` int(11) UNSIGNED NOT NULL,
  `server_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `server_id`) USING BTREE
) ENGINE = MyISAM AVG_ROW_LENGTH = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of psm_users_servers
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
